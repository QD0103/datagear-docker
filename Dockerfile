FROM arm64v8/openjdk:7

WORKDIR /opt
ENV VERSION=2.3.0
ENV PACKAGE=datagear-${VERSION}
ENV ZIP_FILE=${PACKAGE}.zip

RUN curl -O http://www.datagear.tech/download/version/${VERSION}/${ZIP_FILE}

RUN unzip ${ZIP_FILE}

RUN chmod +x /opt/${PACKAGE}/startup.sh
RUN chmod +x /opt/${PACKAGE}/shutdown.sh

#RUN echo "export PATH=$PATH:/opt/${PACKAGE}" >> ~/.bashrc